-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 30, 2019 at 04:35 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SIARR`
--

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id_doc` int(200) NOT NULL,
  `nama_pemilik` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tahun` varchar(20) NOT NULL,
  -- `kategori` varchar(10) NOT NULL,
  `id_kategori` int(200) NOT NULL,
  -- `jenis_kdr` varchar(30) NOT NULL,
  `id_kendaraan` int(20) NOT NULL,
  `keterangan` text NOT NULL,
  `nopolis` varchar(16) NOT NULL,
  `norangka` varchar(30) NOT NULL,
  `nomesin` varchar(50) NOT NULL,
  `name_doc` varchar(100) NOT NULL,
  -- `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
  `lokasi` int(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id_doc`, `nama_pemilik`, `alamat`, `tahun`, `id_kategori`, `id_kendaraan`, `keterangan`, `nopolis`, `norangka`, `nomesin`, `name_doc`, `create_at`,`lokasi`, `creater`) VALUES
(1, 'Muhamad Jumadil Akbar', 'Jl.Manjalega RT.02/16 , Bandung [KEC. MARGACINTA]', '2017', 1, 1, '-', 'BG 1235 GAA', '123456789D', '9876462681JK', 'STNK_BG1235GAA2019-07-24_06_41_08pm.jpg', '2019-07-25 13:55:41', 1, 1),
(2, 'Hendar Sudarma', 'Jl.Manjalega RT.02/16 , Bandung [KEC. MARGACINTA]', '2014', 1, 1, 'mati pajak', 'D 5996 DH', 'MH1JFC110CK004032', 'JBH1E0013461', 'STNK_D5996DH2019-07-24_06_51_31pm.jpg', '2019-07-25 03:28:36', 2, 1),
(3, 'Herman Suherman', 'tangeran selatan', '2014', 2, 1, 'Baru', 'B 5678 AA', 'MH1JFC110CK004032', 'JBH1E0013461', 'BPKB_B5678AA2019-07-25_05_35_01am.jpg', '2019-07-25 13:30:03', 3, 1),
(4, 'Herian Jaya', 'Yogyakarta', '2015', 2, 2, '-', 'AD 5576 B', 'MH1JFC110CK00404', 'JBH1E0013445', 'BPKB_AD5576B2019-07-25_05_48_19am.jpg', '2019-07-25 13:30:07', 1, 1),
(6, 'Fajar Rohman', 'Jl. Kemang Raya, RT.14/RW.1, Bangka, Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12730', '2017', 2, 1, '-', 'B 3203 UNP', 'MF3VR01SCHL000002', 'VR001FMG17000001', 'BPKB_B3203UNP2019-07-25_05_00_10pm.jpg', '2019-07-25 15:00:10', 2, 1),
(11, 'Tahriyatul Kifayati Muaini', 'KP. Jurang Mangu RT.004 RW001, KEL. Jurangmangu Barat, KEC. Pondok Arem, Tangerang Selatan.', '2014', 2, 1, '-', 'B 6588 WLX', 'KH1JFM219EK940435', 'J4M2E1324810', 'BPKB_B6588WLX2019-07-26_01_32_35am.jpg', '2019-07-26 01:07:21', 3, 1),
(13, 'Nurlinah', 'DE LATINOS D-8 NO.17 BSD CITY, RT.012 RW.006 KEL. Serpong, KEC. SERPONG, Tangerang Selatan , Banten', '2010', 2, 2, '-', 'B 1263 NFU', 'MR054HY91A4E57128', '1NZY245882', 'BPKB_B1263NFU2019-07-26_03_13_00am.jpeg', '2019-07-26 01:13:00', 1, 1),
(14, 'Nurlinah', 'DE LATINOS D-8 NO.17 BSD CITY, RT.012 RW.006 KEL. Serpong, KEC. SERPONG, Tangerang Selatan , Banten', '2010', 1, 2, '-', 'B 1263 NFU', '1NZY245882', 'MR054HY91A4E57128', 'STNK_B1263NFU2019-07-26_03_40_24am.jpeg', '2019-07-26 01:40:24', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `Nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `Nama_lengkap`, `email`, `level`) VALUES
(1, 'admin1', 'admin1', 'Admin Bima1', 'admin1@gmail.com', 'Admin'),
(2, 'operator', 'operator', 'Operator Bima', 'operator@akun.com', 'operator'),
(4, 'adil', 'adil', 'jumadil akbar', 'adil@gmail.com', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_doc`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id_doc` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(200) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `creater`) VALUES
(1, 'STNK', 1),
(2, 'BPKB', 1);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(200) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` int(200) NOT NULL,
  `jenis_kdr` varchar(50) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `jenis_kdr`, `creater`) VALUES
(1, 'Sepeda Motor', 1),
(2, 'Mobil', 1);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kendaraan`
  MODIFY `id_kendaraan` int(200) NOT NULL AUTO_INCREMENT;

--
-- SET FOREIGN KEY
--
ALTER TABLE `documents`
  ADD FOREIGN KEY (`id_kategori`) REFERENCES `kategori`(`id_kategori`);
ALTER TABLE `documents`
  ADD FOREIGN KEY (`id_kendaraan`) REFERENCES `kendaraan`(`id_kendaraan`);
ALTER TABLE `documents`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
ALTER TABLE `kategori`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
ALTER TABLE `kendaraan`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
--
-- Table structure for table `restore_documents`
--

CREATE TABLE `restore_documents` (
  `id_doc` int(200) NOT NULL,
  `nama_pemilik` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tahun` varchar(20) NOT NULL,
  -- `kategori` varchar(10) NOT NULL,
  `id_kategori` int(200) NOT NULL,
  -- `jenis_kdr` varchar(30) NOT NULL,
  `id_kendaraan` int(20) NOT NULL,
  `keterangan` text NOT NULL,
  `nopolis` varchar(16) NOT NULL,
  `norangka` varchar(30) NOT NULL,
  `nomesin` varchar(50) NOT NULL,
  `name_doc` varchar(100) NOT NULL,
  -- `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
  `lokasi` int(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `restore_documents`
--
ALTER TABLE `restore_documents`
  ADD PRIMARY KEY (`id_doc`);
--
-- SET FOREIGN KEY
--
ALTER TABLE `restore_documents`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);

-- Table structure for table `gedung`
CREATE TABLE `gedung` (
  `id_gedung` int(200) NOT NULL,
  `nama_gedung` varchar(50) NOT NULL,
  `alamat_gedung` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Table structure for table `rak`
CREATE TABLE `rak` (
  `id_rak` int(200) NOT NULL,
  `nama_rak` varchar(50) NOT NULL,
  `jenis_rak` varchar(50),
  `id_gedung` int(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Table structure for table `posisi`
CREATE TABLE `posisi` (
  `id_posisi` int(200) NOT NULL,
  `baris` varchar(200) NOT NULL,
  `kolom` varchar(200) NOT NULL,
  `id_rak` int(200) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `creater` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table `gedung`
INSERT INTO `gedung` (`id_gedung`, `nama_gedung`, `alamat_gedung`, `creater`) VALUES
(1, 'Pabukon', 'Jl. Pabukon', 1),
(2, 'Wilasa', 'Jl. Wilasa', 1),
(3, 'Sawala', 'Jl. Sawala', 1);
-- Dumping data for table `rak`
INSERT INTO `rak` (`id_rak`, `nama_rak`, `id_gedung`, `creater`) VALUES
(1, 'RAK 1', 1 , 1),
(2, 'RAK 2', 2 , 1),
(3, 'RAK 3', 3 , 1);
-- Dumping data for table `posisi`
INSERT INTO `posisi` (`id_posisi`, `baris`, `kolom`, `id_rak`, `creater`) VALUES
(1, '1', 'A', 1 , 1),
(2, '2', 'B', 2 , 1),
(3, '3', 'C', 3 , 1);

-- Indexes
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`id_gedung`);
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id_rak`);
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id_posisi`);

-- AUTO_INCREMENT
ALTER TABLE `gedung`
  MODIFY `id_gedung` int(200) NOT NULL AUTO_INCREMENT;
ALTER TABLE `rak`
  MODIFY `id_rak` int(200) NOT NULL AUTO_INCREMENT;
ALTER TABLE `posisi`
  MODIFY `id_posisi` int(200) NOT NULL AUTO_INCREMENT;

-- SET FOREIGN KEY
ALTER TABLE `rak`
  ADD FOREIGN KEY (`id_gedung`) REFERENCES `gedung`(`id_gedung`);
ALTER TABLE `posisi`
  ADD FOREIGN KEY (`id_rak`) REFERENCES `rak`(`id_rak`);
ALTER TABLE `gedung`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
ALTER TABLE `rak`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
ALTER TABLE `posisi`
  ADD FOREIGN KEY (`creater`) REFERENCES `user`(`id_user`);
ALTER TABLE `documents`
  ADD FOREIGN KEY (`lokasi`) REFERENCES `posisi`(`id_posisi`);