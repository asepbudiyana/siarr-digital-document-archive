<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintools extends CI_Controller {

    function __construct(){
		parent::__construct();
		  $this->load->helper(array('form', 'url'));
		  $this->load->model('m_admintools');
    }

    public function index(){		
		$data['page'] = 'admintools/list';
		$data['tb_doc'] = $this->db->query("SELECT * FROM user")->result();	
		$this->load->view('administrator/index',$data);
	}

	public function add(){		
		$data['page'] = 'admintools/add';	
		$this->load->view('administrator/index',$data);
	}

	public function do_upload(){	
		$username = $this->input->post('username');	
		$password = $this->input->post('password');
		$Nama_lengkap = $this->input->post('nama_lengkap');	
		$email = $this->input->post('email');	
		$level = $this->input->post('level');	

		$data_doc = array(
			'username' => $username,
			'password' => $password,
			'Nama_lengkap' => $Nama_lengkap,
			'email' => $email,
			'level' => $level
		);
		$this->m_admintools->input_data($data_doc,'user');
		redirect('admintools/index');
	}

	public function edit(){	
		$id	= $this->uri->segment('3');
		$data['page'] = 'admintools/edit';
		$data['tb_doc'] = $this->db->query("SELECT * FROM user WHERE id_user='$id'")->result();	
		$this->load->view('administrator/index',$data);
	}

	public function do_edit(){
		$id = $this->uri->segment('3');
		
		$username = $this->input->post('username');	
		$password = $this->input->post('password');
		$Nama_lengkap = $this->input->post('nama_lengkap');	
		$email = $this->input->post('email');	
		$level = $this->input->post('level');	

		$data_doc = array(
			'username' => $username,
			'password' => $password,
			'Nama_lengkap' => $Nama_lengkap,
			'email' => $email,
			'level' => $level
			);
		$this->m_admintools->update_data($data_doc,$id);
		redirect('admintools/index');
	}

	public function delete(){
		$id = $this->uri->segment('3');
		$this->m_admintools->delete_data($id);
		redirect('admintools/index');
	}
}