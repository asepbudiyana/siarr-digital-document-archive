
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>User</h4>
                    <span>all related user are here</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Admintools</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">List User</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--  <div class="row"> -->
<div class="card"> 
<br>
<div class="container-fluid">
    <form action="<?php echo base_url() ?>index.php/documents/search" method="POST">
            <div class="main-search morphsearch-search">
                <div class="input-group">
                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                    <input type="text" name="search" class="form-control">
                    <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                </div>
            </div>
    </form>
</div>

    <div class="card-header">
        <!-- <h5>Documents Table</h5> -->
        <!-- <span>use class <code>table</code> inside table element</span> -->
        <a href="<?php echo base_url() ?>index.php/admintools/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add User</a>

    </div>
    <!-- <div class="container"> -->
    <div class="card-block table-border-style">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $no = 1;
                foreach ($tb_doc as $value) {
                    # code...
                
                ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?></th>
                        <td><?php echo $value->id_user; ?></td>
                        <td><?php echo $value->username; ?></td>
                        <td><?php echo $value->password; ?></td>
                        <td><?php echo $value->Nama_lengkap; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><?php echo $value->level; ?></td>
                        <td>
                        <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a>
                        <a href="<?php echo base_url() ?>index.php/documents/detail/<?php echo $value->id_doc; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Document Information">
                        <i class="icofont icofont-list"></i></a> -->
                        <form action = "<?php echo base_url() ?>index.php/admintools/delete/<?php echo $value->id_user; ?>" method="POST" enctype="multipart/form-data">
                            <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a> -->
                            <!-- <a href="<?php echo base_url() ?>index.php/admintools/detail/<?php echo $value->id_user; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Document Information">
                            <i class="icofont icofont-list"></i></a> -->
                            <a href="<?php echo base_url() ?>index.php/admintools/edit/<?php echo $value->id_user; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit User">
                            <i class="icofont icofont-ui-edit"></i></a>
                            <!-- <input type="hidden" name="nama_kategori" value=<?php echo $value->nama_kategori ?>> -->
                            <button type="submit" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete User">
                            <i class="icofont icofont-trash"></i></button>
                        </form>
                        </td>
                    </tr>
                    <?php 
                    $no++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    <!-- </div> -->
    </div>
    
</div>
<!-- </div> -->