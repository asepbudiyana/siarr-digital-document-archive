<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <div class="d-inline">
                            <h4>User</h4>
                            <span>pengeditan data user</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Admintools</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Edit User</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5> Form Inputs</h5>
                <span>Harap <code>teliti</code> dalam <code>input</code> data</span>

                <?php foreach ($tb_doc as $doc){ ?>

                <div class="card-header-right">
                    <i class="icofont icofont-spinner-alt-5"></i>
                </div>

            </div>
            <div class="card-block">
                <h4 class="sub-title">User Information</h4>
                <form action = "<?php echo base_url() ?>index.php/admintools/do_edit/<?php echo $doc->id_user ?>" method="POST" enctype="multipart/form-data">
                    
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" name ="username" class="form-control" placeholder="Username" value="<?php echo $doc->username ?>" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name ="password" class="form-control" placeholder="Password" value="<?php echo $doc->password ?>" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" name ="nama_lengkap" class="form-control" placeholder="Nama Lengkap" value="<?php echo $doc->Nama_lengkap ?>" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">e-mail</label>
                        <div class="col-sm-10">
                            <input type="text" name ="email" class="form-control" placeholder="e-mail" value="<?php echo $doc->email ?>" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Level</label>
                        <div class="col-sm-10">
                            <select name="level" class="form-control">
                                <option value="<?php echo $doc->level ?>"><?php echo $doc->level ?></option>
                                <option value="Admin">Admin</option>
                                <option value="Operator">Operator</option>
                            </select>
                        </div>
                    </div>
                            <button type="submit" class="btn btn-success m-b-0">Submit</button>
                        </form>
                <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>