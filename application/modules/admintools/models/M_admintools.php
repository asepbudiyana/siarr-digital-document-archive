<?php 

class M_admintools extends CI_Model {
	function tampil_data(){
		return $this->db->get('user');
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function update_data($data, $id){
        $this->db->where('id_user', $id);
        $this->db->limit(1);
        $this->db->update('user', $data);
	}

	function delete_data($id){
        $this->db->where('id_user', $id);
        $this->db->delete('user');
    }
}