<?php

class M_masterdata extends CI_Model {
    function tampil_data(){
        return $this->db->get('kategori');
    }

    function input_data($data,$table){
        $this->db->insert($table,$data);
    }

    function update_data($data, $table, $id){
        $this->db->where('id_'.$table, $id);
        $this->db->limit(1);
        $this->db->update($table, $data);
    }

    function delete_data($table, $id){
        $this->db->where('id_'.$table, $id);
        $this->db->delete($table);
    }
}