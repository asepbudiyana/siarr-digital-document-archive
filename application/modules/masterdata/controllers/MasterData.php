<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterData extends CI_Controller {

    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
          $this->load->model('m_masterdata');
          
    }

    public function index(){
        $data['page'] = 'masterdata/list';
        $data['tb_md'] = $this->db->query("SELECT * FROM kategori ORDER BY nama_kategori ASC")->result();
        $data['tb_kdr'] = $this->db->query("SELECT * FROM kendaraan ORDER BY jenis_kdr ASC")->result();	
        $data['tb_gdg'] = $this->db->query("SELECT * FROM gedung ORDER BY nama_gedung ASC")->result();	
		$this->load->view('administrator/index',$data);
    }

    public function add(){
        $subdata = $this->uri->segment('3');
        $data['page'] = 'masterdata/'.$subdata.'_add';
        $this->load->view('administrator/index',$data);
    }

    public function do_upload(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');
        $creater = $this->input->post('creater');
        if ($subdata == 'kategori') {
            $nama_kategori = $this->input->post('nama_kategori');

            $data = array(
                'nama_kategori' => $nama_kategori,
                'creater' => $creater
            );
            mkdir('./uploads/'.$nama_kategori."/");
        }
        else if ($subdata == 'kendaraan') {
            $jenis_kdr = $this->input->post('jenis_kdr');

            $data = array(
                'jenis_kdr' => $jenis_kdr,
                'creater' => $creater
            );
        }
        else if ($subdata == 'gedung'){
            $nama_gedung = $this->input->post('nama');
            $alamat_gedung = $this->input->post('alamat');

            $data = array(
                'nama_gedung' => $nama_gedung,
                'alamat_gedung' => $alamat_gedung,
                'creater' => $creater
            );  
        }
        else if ($subdata == 'rak'){
            $nama_rak = $this->input->post('nama');
            $jenis_rak = $this->input->post('jenis');

            $data = array(
                'nama_rak' => $nama_rak,
                'jenis_rak' => $jenis_rak,
                'id_gedung' => $id,
                'creater' => $creater
            );
        }
        else if ($subdata == 'posisi'){
            $baris = $this->input->post('baris');
            $kolom = $this->input->post('kolom');

            $data = array(
                'baris' => $baris,
                'kolom' => $kolom,
                'id_rak' => $id,
                'creater' => $creater
            );
        }
        
        $this->m_masterdata->input_data($data,$subdata);
        redirect('masterdata/index');
    }

    public function detail(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');
        $sub = substr($subdata,0,2);
        // $data['tb_doc'] = $this->db->query("SELECT * FROM documents NATURAL JOIN kategori NATURAL JOIN kendaraan WHERE id_$subdata ='$id' ORDER BY tanggal DESC")->result();	
        $data['tb_doc'] = $this->db->query("SELECT * FROM documents AS D, kategori AS Ka, kendaraan AS Ke, posisi AS po WHERE D.id_kategori = Ka.id_kategori AND D.id_kendaraan = Ke.id_kendaraan AND D.lokasi = po.id_posisi AND $sub.id_$subdata = '$id' ORDER BY D.update_at DESC")->result();	
        // $data['tb_rak'] = $this->db->query("SELECT * FROM rak WHERE id_$subdata = '$id'")->result();
        $data['page'] = 'masterdata/'.$subdata.'_detail';
        $this->load->view('administrator/index',$data);
    }
//lokasi detail start
    public function detailLokasi(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');
        $sub = substr($subdata,0,2);
        if ($subdata=='gedung') $ambil = 'rak';
        else if ($subdata=='rak') $ambil = 'posisi';
        // $data['tb_doc'] = $this->db->query("SELECT * FROM documents NATURAL JOIN kategori NATURAL JOIN kendaraan WHERE id_$subdata ='$id' ORDER BY tanggal DESC")->result();	
        // $data['tb_doc'] = $this->db->query("SELECT * FROM documents AS D, kategori AS Ka, kendaraan AS Ke WHERE D.id_kategori = Ka.id_kategori AND D.id_kendaraan = Ke.id_kendaraan AND $sub.id_$subdata = '$id' ORDER BY D.update_at DESC")->result();	
        $data['tb_loc'] = $this->db->query("SELECT * FROM $ambil WHERE id_$subdata = '$id'")->result();
        $data['page'] = 'masterdata/'.$subdata.'_detail';
        $this->load->view('administrator/index',$data);
    }
//lokasi detail end

    public function update(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');
        $data['page'] = 'masterdata/'.$subdata.'_update';
        $data['tb_kategori'] = $this->db->query("SELECT * FROM kategori WHERE id_kategori = '$id'")->result();
        $data['tb_kendaraan'] = $this->db->query("SELECT * FROM kendaraan WHERE id_kendaraan = '$id'")->result();
        $data['tb_gedung'] = $this->db->query("SELECT * FROM gedung WHERE id_gedung = '$id'")->result();
        $data['tb_rak'] = $this->db->query("SELECT * FROM rak WHERE id_rak = '$id'")->result();
        $data['tb_posisi'] = $this->db->query("SELECT * FROM posisi WHERE id_posisi = '$id'")->result();
        $this->load->view('administrator/index',$data);
    }

    public function do_update(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');

        if ($subdata == 'kategori'){
            $nama_kategori = $this->input->post('nama_kategori');
            $kategori_lama = $this->input->post('kategori_lama');

            $data = array(
                'nama_kategori' => $nama_kategori
            );
            rename('./uploads/'.$kategori_lama."/",'./uploads/'.$nama_kategori."/");
        }
        else if ($subdata == 'kendaraan'){
            $jenis_kdr = $this->input->post('jenis_kdr');

            $data = array(
                'jenis_kdr' => $jenis_kdr
            );
        }
        else if ($subdata == 'gedung'){
            $nama_gedung = $this->input->post('nama');
            $alamat_gedung = $this->input->post('alamat');

            $data = array(
                'nama_gedung' => $nama_gedung,
                'alamat_gedung' => $alamat_gedung,
            );  
        }
        else if ($subdata == 'rak'){
            $nama_rak = $this->input->post('nama');
            $jenis_rak = $this->input->post('jenis');

            $data = array(
                'nama_rak' => $nama_rak,
                'jenis_rak' => $jenis_rak,
            );
        }
        else if ($subdata == 'posisi'){
            $baris = $this->input->post('baris');
            $kolom = $this->input->post('kolom');

            $data = array(
                'baris' => $baris,
                'kolom' => $kolom,
            );
        }
        $this->m_masterdata->update_data($data,$subdata,$id);
        redirect('masterdata/index');
    }

    public function delete(){
        $subdata = $this->uri->segment('3');
        $id = $this->uri->segment('4');

        if ($subdata == 'kategori'){
            $nama_kategori = $this->input->post('nama_kategori');
            rmdir('./uploads/'.$nama_kategori."/");
        }
        $this->m_masterdata->delete_data($subdata,$id);
        redirect('masterdata/index');	
    }
}