<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Jenis Kendaraan</h4>
                    <span>Detail Jenis Kendaraan</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Master Data</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Detail Jenis Kendaraan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
                                    
    <div class="col-xl-12 col-md-12">
        <!-- Bootstrap tab card start -->
        <div class="card">
            <div class="card-header">
                    <a href="<?php echo base_url() ?>index.php/masterdata/add/rak/<?php echo $this->uri->segment('4'); ?>" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Rak</a>
            </div>
            <div class="card-block">
                <!-- Row start -->
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <!-- <div class="card-header">Daftar Dokumen</div> -->
                            <!-- <div class="card-block table-border-style"> -->
                                    <!-- <div class="table-responsive"> -->
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>ID</th>
                                                    <th>Nama Rak</th>
                                                    <th>Jenis Rak</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            $no = 1;
                                            foreach ($tb_loc as $value) {
                                                # code...
                                            
                                            ?>
                                                <tr>
                                                    <th scope="row"><?php echo $no; ?></th>
                                                    <td><?php echo $value->id_rak; ?></td>
                                                    <td><?php echo $value->nama_rak; ?></td>
                                                    <td><?php echo $value->jenis_rak; ?></td>
                                                    <td>
                                                    <form action = "<?php echo base_url() ?>index.php/masterdata/delete/rak/<?php echo $value->id_rak; ?>" method="POST" enctype="multipart/form-data">
                                                        <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a> -->
                                                        <a href="<?php echo base_url() ?>index.php/masterdata/detailLokasi/rak/<?php echo $value->id_rak; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Document Information">
                                                        <i class="icofont icofont-list"></i></a>
                                                        <a href="<?php echo base_url() ?>index.php/masterdata/update/rak/<?php echo $value->id_rak; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit Document">
                                                        <i class="icofont icofont-ui-edit"></i></a>
                                                        <input type="hidden" name="nama_kategori" value=<?php echo $value->nama_rak ?>>
                                                        <button type="submit" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete Document">
                                                        <i class="icofont icofont-trash"></i></button>
                                                    </form>
                                                    </td>
                                                </tr>
                                                <?php 
                                                $no++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <!-- </div> -->
                                </div>    
                        </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
        </div>
        <!-- Bootstrap tab card end -->
    </div>
</div>