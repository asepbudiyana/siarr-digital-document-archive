
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Master Data</h4>
                    <span>all related master data are here</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Master Data</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">List Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-8 col-xl-12">
<!-- <h6 class="sub-title">Tab With Icon</h6> -->                                                                
<!-- Nav tabs -->                           
<div class="card">                           
    <ul class="nav nav-tabs md-tabs" role="tablist">
        <li class="nav-item">                                                                
            <a class="nav-link active" data-toggle="tab" href="#kategori" role="tab"><i class="icofont icofont-ui-folder"></i> Folder / Kategori</a>                                                                        
            <div class="slide"></div>
        </li>
        <li class="nav-item">                                                                
            <a class="nav-link" data-toggle="tab" href="#lokasi" role="tab"><i class="icofont icofont-location-pin"></i> Lokasi</a>                                                                        
            <div class="slide"></div>
        </li>
        <li class="nav-item">                                                                
            <a class="nav-link" data-toggle="tab" href="#kendaraan" role="tab"><i class="icofont icofont-vehicle-delivery-van"></i> Jenis Kendaraan</a>                                                                        
            <div class="slide"></div>
        </li>
    </ul>                                                              
<!-- Tab panes -->
    <div class="tab-content card-block">
        <div class="tab-pane active" id="kategori" role="tabpanel">
             <div class="card"> 
                <div class="card-header">
                    <a href="<?php echo base_url() ?>index.php/masterdata/add/kategori" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Folder / Category</a>
                </div>
                <!-- <div class="container"> -->
                <div class="card-block table-border-style">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama Folder / Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 1;
                            foreach ($tb_md as $value) {
                                # code...
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $no; ?></th>
                                    <td><?php echo $value->id_kategori ?></td>
                                    <td><?php echo $value->nama_kategori; ?></td>
                                    <td>
                                    <form action = "<?php echo base_url() ?>index.php/masterdata/delete/kategori/<?php echo $value->id_kategori; ?>" method="POST" enctype="multipart/form-data">
                                        <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a> -->
                                        <a href="<?php echo base_url() ?>index.php/masterdata/detail/kategori/<?php echo $value->id_kategori; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="List Document">
                                        <i class="icofont icofont-list"></i></a>
                                        <a href="<?php echo base_url() ?>index.php/masterdata/update/kategori/<?php echo $value->id_kategori; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit Category Name">
                                        <i class="icofont icofont-ui-edit"></i></a>
                                        <input type="hidden" name="nama_kategori" value=<?php echo $value->nama_kategori ?>>
                                        <button type="submit" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete Category">
                                        <i class="icofont icofont-trash"></i></button>
                                    </form>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <!-- </div> -->
                </div>
                
            </div>
            <!-- </div> -->
        </div>
        <div class="tab-pane" id="lokasi" role="tabpanel">
            <div class="card"> 
                <div class="card-header">
                    <a href="<?php echo base_url() ?>index.php/masterdata/add/gedung" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Gedung</a>
                </div>
                <!-- <div class="container"> -->
                <div class="card-block table-border-style">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama Gedung</th>
                                    <th>Alamat Gedung</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 1;
                            foreach ($tb_gdg as $value) {
                                # code...
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $no; ?></th>
                                    <td><?php echo $value->id_gedung ?></td>
                                    <td><?php echo $value->nama_gedung; ?></td>
                                    <td><?php echo $value->alamat_gedung; ?></td>
                                    <td>
                                    <form action = "<?php echo base_url() ?>index.php/masterdata/delete/gedung/<?php echo $value->id_gedung; ?>" method="POST" enctype="multipart/form-data">
                                        <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a> -->
                                        <a href="<?php echo base_url() ?>index.php/masterdata/detailLokasi/gedung/<?php echo $value->id_gedung; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="List Document">
                                        <i class="icofont icofont-list"></i></a>
                                        <a href="<?php echo base_url() ?>index.php/masterdata/update/gedung/<?php echo $value->id_gedung; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit Category Name">
                                        <i class="icofont icofont-ui-edit"></i></a>
                                        <input type="hidden" name="nama_kategori" value=<?php echo $value->nama_gedung; ?>>
                                        <button type="submit" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete Category">
                                        <i class="icofont icofont-trash"></i></button>
                                    </form>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <!-- </div> -->
                </div>
                
            </div>
            <!-- </div> -->
        </div>
        <div class="tab-pane" id="kendaraan" role="tabpanel">
            <div class="card"> 
                <div class="card-header">
                    <a href="<?php echo base_url() ?>index.php/masterdata/add/kendaraan" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Vehicle Type</a>
                </div>
                <!-- <div class="container"> -->
                <div class="card-block table-border-style">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 1;
                            foreach ($tb_kdr as $value) {
                                # code...
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $no; ?></th>
                                    <td><?php echo $value->id_kendaraan ?></td>
                                    <td><?php echo $value->jenis_kdr; ?></td>
                                    <td>
                                    <form action = "<?php echo base_url() ?>index.php/masterdata/delete/kendaraan/<?php echo $value->id_kendaraan; ?>" method="POST" enctype="multipart/form-data">
                                        <!-- <a href="<?php echo base_url() ?>index.php/documents/add" class="btn btn-success btn-outline-success"><i class="icofont icofont-plus"></i> Add Document</a> -->
                                        <a href="<?php echo base_url() ?>index.php/masterdata/detail/kendaraan/<?php echo $value->id_kendaraan; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="List Document">
                                        <i class="icofont icofont-list"></i></a>
                                        <a href="<?php echo base_url() ?>index.php/masterdata/update/kendaraan/<?php echo $value->id_kendaraan; ?>" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit Vehicle Type">
                                        <i class="icofont icofont-ui-edit"></i></a>
                                        <input type="hidden" name="nama_kategori" value=<?php echo $value->jenis_kdr ?>>
                                        <button type="submit" class="btn btn-primary btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete Vehicle Type">
                                        <i class="icofont icofont-trash"></i></button>
                                    </form>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <!-- </div> -->
                </div>
                
</div>
        </div>
    </div>
</div>