<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <div class="d-inline">
                            <h4>Folder / Kategori</h4>
                            <span>penambahan data baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Add Kategori</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5> Form Input</h5>
                <span>Harap <code>teliti</code> dalam <code>input</code> data</span>


                <div class="card-header-right">
                    <i class="icofont icofont-spinner-alt-5"></i>
                </div>

            </div>
            <div class="card-block">
                <h4 class="sub-title">Informasi Folder / Kategori</h4>
                <form action = "<?php echo base_url() ?>index.php/masterdata/do_upload/kategori" method="POST" enctype="multipart/form-data">
                    
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Folder / Kategori</label>
                        <div class="input-group input-group-button col-sm-10">
                            <input type="hidden" name="creater" value="<?php echo $this->session->userdata('id_user'); ?>">
                            <input type="text" name ="nama_kategori" class="form-control" placeholder="Nama Folder / Kategori" required>
                            <button type="submit" class="btn btn-success m-b-0">Submit</button>
                        </div>
                    </div> 
                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>