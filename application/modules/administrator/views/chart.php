
<!doctype html>
<html>

<head>
	<title>Combo Bar-Line Chart</title>
	<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>
	<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
	<style>
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	</style>
</head>

<body>
	<div style="width: 77%">
		<canvas id="canvas"></canvas>
	</div>
	<!-- <button id="randomizeData">Randomize Data</button> -->
	<script>
    function getData(cData, cMonth){
      var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      for (let i = 0; i < cData.length; i++){
        data[cMonth[i]-1] = cData[i];
      }
      return data;
    }
    var cData = JSON.parse(`<?php echo $chart_data; ?>`);
		var chartData = {
			labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			datasets: [{
				type: 'bar',
				label: 'Data Masuk',
				backgroundColor: window.chartColors.green,
				borderWidth: 2,
				data: getData(cData.data, cData.month)
			}, {
				type: 'bar',
				label: 'Data Keluar',
				backgroundColor: window.chartColors.red,
				data: getData(cData.data2, cData.month2),
				borderWidth: 2
      }]
			// }, {
			// 	type: 'bar',
			// 	label: cData.label,
			// 	backgroundColor: window.chartColors.green,
			// 	data: [
			// 		randomScalingFactor(),
			// 		randomScalingFactor(),
			// 		randomScalingFactor(),
			// 		randomScalingFactor(),
			// 		randomScalingFactor(),
			// 		randomScalingFactor(),
			// 		randomScalingFactor()
			// 	]
			// }]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myMixedChart = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Grafik Aktivitas Admin'
					},
					tooltips: {
						mode: 'index',
						intersect: true
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			chartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myMixedChart.update();
		});
	</script>
</body>

</html>

<!-- <!DOCTYPE html>
<html>
<head>
  <title>ChartJS - bar</title> -->
  <!-- Latest CSS -->
 <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
  <div class="chart-container">
    <div class="bar-chart-container" style="width:75% height;">
      <canvas id="bar-chart"></canvas>
    </div>
  </div> -->
 
  <!-- javascript -->
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
</body> -->
<script>
  // $(function(){
  //     //get the bar chart canvas
  //     var cData = JSON.parse(`<?php echo $chart_data; ?>`);
  //     var ctx = $("#bar-chart");
 
  //     //bar chart data
  //     var data = {
  //       labels: cData.label,
  //       datasets: [
  //         {
  //           label: cData.label,
  //           data: cData.data,
  //           backgroundColor: [
  //             "#DEB887",
  //             "#A9A9A9",
  //             "#DC143C",
  //             "#F4A460",
  //             "#2E8B57",
  //             "#1D7A46",
  //             "#CDA776",
  //             "#CDA776",
  //             "#989898",
  //             "#CB252B",
  //             "#E39371",
  //           ],
  //           borderColor: [
  //             "#CDA776",
  //             "#989898",
  //             "#CB252B",
  //             "#E39371",
  //             "#1D7A46",
  //             "#F4A460",
  //             "#CDA776",
  //             "#DEB887",
  //             "#A9A9A9",
  //             "#DC143C",
  //             "#F4A460",
  //             "#2E8B57",
  //           ],
  //           borderWidth: [1, 1, 1, 1, 1,1,1,1, 1, 1, 1,1,1]
  //         }
  //       ]
  //     };
 
  //     //options
  //     var options = {
  //       responsive: true,
  //       title: {
  //         display: true,
  //         position: "top",
  //         text: "Grafik Aktifitas Admin",
  //         fontSize: 18,
  //         fontColor: "#111"
  //       },
  //       legend: {
  //         display: true,
  //         position: "bottom",
  //         labels: {
  //           fontColor: "#333",
  //           fontSize: 16
  //         }
  //       }
  //     };
 
  //     //create bar Chart class object
  //     var chart1 = new Chart(ctx, {
  //       type: "bar",
  //       data: data,
  //       options: options
  //     });
 
  // });
</script>
<!-- </html> -->