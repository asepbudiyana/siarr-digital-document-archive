<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar">
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="<?php echo base_url() ?>index.php/administrator">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
                
            </li>
            <li class="pcoded-hasmenu">
                <a href="<?php echo base_url() ?>index.php/documents">
                    <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                    <span class="pcoded-mtext">Documents</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="<?php echo base_url() ?>index.php/masterdata">
                <!-- <a href="#"> -->
                    <span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
                    <span class="pcoded-mtext">Master Data</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <!-- <a href="<?php echo base_url() ?>index.php/access"> -->
                <a href="#">

                    <span class="pcoded-micon"><i class="feather icon-credit-card"></i></span>
                    <span class="pcoded-mtext">Access</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <!-- <a href="<?php echo base_url() ?>index.php/notifikasi"> -->
                <a href="#">

                    <span class="pcoded-micon"><i class="feather icon-map-pin"></i></span>
                    <span class="pcoded-mtext">Notification List</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
                
            </li>
            <li class="pcoded-hasmenu">
                <!-- <a href="<?php echo base_url() ?>index.php/calender"> -->
                <a href="#">

                    <span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
                    <span class="pcoded-mtext">Calendar</span>
                    <!-- <span class="pcoded-mcaret"></span> -->
                </a>
                
            </li>
            <li class="pcoded-hasmenu">
                <a href="<?php echo base_url() ?>index.php/admintools">
                <!-- <a href="#"> -->

                    <span class="pcoded-micon"><i class="feather icon-message-square"></i></span>
                    <span class="pcoded-mtext">Admin-Tools</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="chat.htm" data-i18n="nav.chat.main">
                            <span class="pcoded-micon"><i class="ti-comments"></i></span>
                            <span class="pcoded-mtext">Chat</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>

                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.social.main">
                            <span class="pcoded-micon"><i class="ti-dribbble"></i></span>
                            <span class="pcoded-mtext">Social</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="fb-wall.htm" data-i18n="nav.social.fb-wall">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Wall</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="message.htm" data-i18n="nav.social.messages">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Messages</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.task.main">
                            <span class="pcoded-micon"><i class="ti-check-box"></i></span>
                            <span class="pcoded-mtext">Task</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="task-list.htm" data-i18n="nav.task.task-list">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Task List</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="task-board.htm" data-i18n="nav.task.task-board">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Task Board</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="task-detail.htm" data-i18n="nav.task.task-detail">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Task Detail</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="issue-list.htm" data-i18n="nav.task.issue list">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Issue List</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.to-do.main">
                            <span class="pcoded-micon"><i class="ti-notepad"></i></span>
                            <span class="pcoded-mtext">To-Do</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="todo.htm" data-i18n="nav.to-do.todo">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">To-Do</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="notes.htm" data-i18n="nav.to-do.notes">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Notes</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.gallery.main">
                            <span class="pcoded-micon"><i class="ti-gallery"></i></span>
                            <span class="pcoded-mtext">Gallery</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="gallery-grid.htm" data-i18n="nav.gallery.gallery-grid">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Gallery-Grid</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="gallery-masonry.htm" data-i18n="nav.gallery.masonry-gallery">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Masonry Gallery</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="gallery-advance.htm" data-i18n="nav.gallery.advance-gallery">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Advance Gallery</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.search.main">
                            <span class="pcoded-micon"><i class="ti-search"></i></span>
                            <span class="pcoded-mtext">Search</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="search-result.htm" data-i18n="nav.search.simple-search">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Simple Search</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="search-result2.htm" data-i18n="nav.search.grouping-search">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Grouping Search</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)" data-i18n="nav.job-search.main">
                            <span class="pcoded-micon"><i class="ti-medall-alt"></i></span>
                            <span class="pcoded-mtext">Job Search</span>
                            <span class="pcoded-badge label label-danger">NEW</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="job-card-view.htm" data-i18n="nav.job-search.card-view">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Card View</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="job-details.htm" data-i18n="nav.job-search.job-detailed">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Job Detailed</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="job-find.htm" data-i18n="nav.job-search.job-find">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Job Find</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="job-panel-view.htm" data-i18n="nav.job-search.job-panel-view">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Job Panel View</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><b>E</b></span>
                            <span class="pcoded-mtext">Extension</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="pcoded-hasmenu ">
                                <a href="javascript:void(0)" data-i18n="nav.editor.main">
                                    <span class="pcoded-micon"><i class="ti-pencil-alt"></i></span>
                                    <span class="pcoded-mtext">Editor</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="">
                                        <a href="ck-editor.htm" data-i18n="nav.editor.ck-editor">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">CK-Editor</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="wysiwyg-editor.htm" data-i18n="nav.editor.wysiwyg-editor">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">WYSIWYG Editor</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="ace-editor.htm" data-i18n="nav.editor.ace-editor">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Ace Editor</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>

                                    <li class="">
                                        <a href="long-press-editor.htm" data-i18n="nav.editor.long-press-editor">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Long Press Editor</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="pcoded-hasmenu ">
                                <a href="javascript:void(0)" data-i18n="nav.invoice.main">
                                    <span class="pcoded-micon"><i class="ti-layout-media-right"></i></span>
                                    <span class="pcoded-mtext">Invoice</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="">
                                        <a href="invoice.htm" data-i18n="nav.invoice.invoice">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Invoice</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="invoice-summary.htm" data-i18n="nav.invoice.invoice-summery">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Invoice Summary</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="invoice-list.htm" data-i18n="nav.invoice.invoice-list">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Invoice List</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="pcoded-hasmenu ">
                                <a href="javascript:void(0)" data-i18n="nav.event-calendar.main">
                                    <span class="pcoded-micon"><i class="ti-calendar"></i></span>
                                    <span class="pcoded-mtext">Event Calendar</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="">
                                        <a href="event-full-calender.htm" data-i18n="nav.full-calendar.full-calendar">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Full Calendar</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="event-clndr.htm" data-i18n="nav.clnder.clnder">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">CLNDER</span>
                                            <span class="pcoded-badge label label-info">NEW</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="">
                                <a href="image-crop.htm" data-i18n="nav.image-cropper.main">
                                    <span class="pcoded-micon"><i class="ti-cut"></i></span>
                                    <span class="pcoded-mtext">Image Cropper</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="file-upload.htm" data-i18n="nav.file-upload.main">
                                    <span class="pcoded-micon"><i class="ti-cloud-up"></i></span>
                                    <span class="pcoded-mtext">File Upload</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu ">
                                <a href="javascript:void(0)" data-i18n="nav.internationalize.main">
                                    <span class="pcoded-micon"><i class="ti-world"></i></span>
                                    <span class="pcoded-mtext">Internationalize</span>
                                    <span class="pcoded-badge label label-danger">HOT</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="">
                                        <a href="internationalization/internationalization-after-init.html" data-i18n="nav.internationalize.after-init">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">After Init</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="internationalization/internationalization-fallback.html" data-i18n="nav.internationalize.fallback">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Fallback</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="internationalization/internationalization-on-init.html" data-i18n="nav.internationalize.on-int">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">On Init</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="internationalization/internationalization-resources.html" data-i18n="nav.internationalize.resources">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">Resources</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="internationalization/internationalization-xhr-backend.html" data-i18n="nav.internationalize.xhr-backend">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext">XHR Backend</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="">
                                <a href="change-loges.htm" data-i18n="nav.change-loges.main">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Change Loges</span>
                                    <span class="pcoded-badge label label-warning">1.0</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>