

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card bg-c-yellow text-white">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col">
                                <p class="m-b-5">Jumlah Admin</p>
                                <h4 class="m-b-0"><?php echo $jumlah_admin;?></h4>
                            </div>
                            <div class="col col-auto text-right">
                                <i class="feather icon-user f-50 text-c-yellow"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <div class="col-xl-3 col-md-6">
                <div class="card bg-c-green text-white">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col">
                                <p class="m-b-5">Data Masuk</p>
                                <h4 class="m-b-0"><?php echo $data_masuk;?></h4>
                            </div>
                            <div class="col col-auto text-right">
                                <i class="feather icon-credit-card f-50 text-c-green"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <div class="col-xl-3 col-md-6">
                <div class="card bg-c-pink text-white">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col">
                                <p class="m-b-5">Jumlah Folder / Kategori</p>
                                <h4 class="m-b-0"><?php echo $jumlah_folder;?></h4>
                            </div>
                            <div class="col col-auto text-right">
                                <i class="feather icon-book f-50 text-c-pink"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <div class="col-xl-3 col-md-6">
                <div class="card bg-c-blue text-white">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col">
                                <p class="m-b-5">Data Keluar</p>
                                <h4 class="m-b-0"><?php echo $data_keluar;?></h4>
                            </div>
                            <div class="col col-auto text-right">
                                <i class="feather icon-shopping-cart f-50 text-c-blue"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <div class="col-xl-3 col-md-12">
                <div class="card user-activity-card">
                    <div class="card-header">
                        <h5>Aktivitas User</h5>
                    </div>
                    <div class="card-block">
                    <?php
                    foreach ($tb_doc as $doc) {
                        $doc->name_doc;
                    }
                    foreach ($tb_doc as $value) {
                        # code...
                        if ($value->nama_kategori=='STNK') {
                            $path = "uploads/STNK/";
                        }
                        else if ($value->nama_kategori=='BPKB') {
                            $path = "uploads/BPKB/";                                        
                        }
                    
                    ?>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="user image" class="img-radius cover-img">
                                    <!-- <img src="<?php echo base_url() ?>asset/files/assets/images/avatar-2.jpg" alt="user image" class="img-radius profile-img"> -->
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5"><?php echo $value->nama_pemilik; ?></h6>
                                <p class="text-muted m-b-0"><?php echo $value->nopolis; ?></p>
                                <?php

                                ?>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>
                                <?php 
                                    //>counter update time
                                    $awal  = date_create();
                                    $akhir = date_create($value->uptodate);
                                    $diff  = date_diff( $awal, $akhir );
                                    if($diff->y) echo $diff->y . ' tahun ';
                                    elseif($diff->m) echo $diff->m . ' bulan ';
                                    elseif($diff->d) echo $diff->d . ' hari ';
                                    elseif($diff->h) echo $diff->h . ' jam ';
                                    elseif($diff->i) echo $diff->i . ' menit ';
                                    else echo $diff->s . ' detik ';
                                    echo ' yang lalu';                                    
                                ?></p>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="<?php echo base_url() ?>asset/files/assets/images/breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                    <img src="<?php echo base_url() ?>asset/files/assets/images/avatar-2.jpg" alt="user image" class="img-radius profile-img">
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">John Deo</h6>
                                <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-auto p-r-0">
                                <div class="u-img">
                                    <img src="<?php echo base_url() ?>asset/files/assets/images/breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                    <img src="<?php echo base_url() ?>asset/files/assets/images/avatar-2.jpg" alt="user image" class="img-radius profile-img">
                                </div>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">John Deo</h6>
                                <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                                <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#!" class="b-b-primary text-primary">View all Projects</a>
                        </div> -->
                    </div>
                </div>
            </div>

            <!-- end -->
            <div class="col-xl-9 col-md-12">
                <div class="card user-activity-card">
                    <!-- <div class="card-header"> -->
            <div class="col-xl-8" style="left:80px;">
                <iframe src="<?php echo base_url() ?>index.php/administrator/bar_chart" height="375" width="960" style="border:none;"></iframe>
            </div>
            </div>
            </div>
            </div>
<!-- 
            <div class="col-xl-4 col-md-12">
                <div class="card feed-card">
                    <div class="card-header">
                        <h5>Feeds</h5>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-bell bg-simple-c-blue feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">You have 3 pending tasks. <span class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-shopping-cart bg-simple-c-pink feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New order received <span class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-file-text bg-simple-c-green feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">You have 3 pending tasks. <span class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-shopping-cart bg-simple-c-pink feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">New order received <span class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-auto p-r-0">
                                <i class="feather icon-file-text bg-simple-c-green feed-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-5">You have 3 pending tasks. <span class="text-muted f-right f-13">Just Now</span></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- end -->
        </div>
    