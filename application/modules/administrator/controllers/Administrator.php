<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_administrator');
	}
 
	public function index(){
		$data['tb_doc'] = $this->db->query("SELECT *, D.update_at AS uptodate FROM documents AS D LEFT JOIN kategori AS K ON D.id_kategori = K.id_kategori  ORDER BY D.update_at DESC LIMIT 3")->result();
		$data['page'] = 'administrator/dashboard';
		//> counter
		$data['jumlah_folder'] = $this->db->query("SELECT * FROM kategori")->num_rows();
		$data['jumlah_admin'] = $this->db->query("SELECT * FROM user")->num_rows();
		//> data masuk & data keluar di update per hari
		// $data['data_masuk'] = $this->db->query("SELECT * FROM documents WHERE YEAR(create_at) = YEAR(NOW())")->num_rows();
		$data['data_masuk'] = $this->db->query("SELECT * FROM documents")->num_rows();
		$data['data_keluar'] = $this->db->query("SELECT * FROM restore_documents")->num_rows();
		//$data['data_keluar']
		$this->load->view('administrator/index',$data);
	}
	
	//> percobaan buat chart
	public function bar_chart() {
   
		$query =  $this->db->query("SELECT COUNT(id_doc) as count,MONTHNAME(create_at) as month_name, MONTH(create_at) as month FROM documents WHERE YEAR(create_at) = '" . date('Y') . "'
		GROUP BY YEAR(create_at),MONTH(create_at)"); 
   
		$record = $query->result();
		
		$query2 =  $this->db->query("SELECT COUNT(id_doc) as count,MONTHNAME(create_at) as month_name, MONTH(create_at) as month FROM restore_documents WHERE YEAR(create_at) = '" . date('Y') . "'
		GROUP BY YEAR(create_at),MONTH(create_at)"); 
   
		$record2 = $query2->result();
		$data = [];
   
		foreach($record as $row) {
			  $data['label'][] = $row->month_name;
			  $data['data'][] = (int) $row->count;
			  $data['month'][] = $row->month;

		}
		foreach($record2 as $row) {
			$data['label2'][] = $row->month_name;
			$data['data2'][] = (int) $row->count;
			$data['month2'][] = $row->month;	
		}
		$data['chart_data'] = json_encode($data);
		$this->load->view('administrator/chart',$data);
	}	
}
