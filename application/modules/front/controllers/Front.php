<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_front');
	}
 
	public function index(){		
		$this->load->view('front/index');
	}
	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password
			);
		$cek = $this->m_front->cek_login("user",$where)->num_rows();

		// var_dump($cek);
		if($cek > 0){
			// $temp= $this->db->query("SELECT * FROM user WHERE username=".$username)->result();
			$temp = $this->db->query("SELECT * FROM user WHERE username='$username'")->result();

			foreach($temp as $value) {
				$level = $value->level;
				$id_user = $value->id_user;
			}
			// echo $level;
			$data_session = array(
				'level' =>$level,
				'username' => $username,
				'id_user' => $id_user,
				'status' => "login"
				);
				// var_dump($temp);
			$this->session->set_userdata($data_session);
			$this->session->set_userdata(array('username'=>$username));
			$this->session->set_userdata(array('level'=>$level));
			$this->session->set_userdata(array('id_user'=>$id_user));

			redirect(base_url("index.php/administrator"));
			// echo "berhasil";

		}else{
			// echo "Username dan password salah !";
			$a['val'] = 'gagal';
			// $this->load->view('login',$a);
			redirect(base_url("index.php/"));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/'));
	}
 
	
}
