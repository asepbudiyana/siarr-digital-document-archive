<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		  $this->load->helper(array('form', 'url'));
		  $this->load->model('m_document');

	}
 
	public function index(){		
		$data['page'] = 'documents/list';
		$data['tb_doc'] = $this->db->query("SELECT * FROM documents AS D LEFT JOIN kategori AS K ON D.id_kategori = K.id_kategori ORDER BY D.update_at DESC")->result();	
		$this->load->view('administrator/index',$data);
	}

	public function add(){		
		$data['page'] = 'documents/add';	
		$data['tb_kategori'] = $this->db->query("SELECT * FROM kategori")->result();
		$data['tb_kendaraan'] = $this->db->query("SELECT * FROM kendaraan")->result();
		$data['tb_gedung'] = $this->db->query("SELECT * FROM gedung")->result();
		$data['tb_rak'] = $this->db->query("SELECT * FROM rak")->result();
		$data['tb_posisi'] = $this->db->query("SELECT * FROM posisi")->result();

		// $data['gedung'] = $this->m_document->get_category()->result();
		$data['cities'] = $this->m_document->getGedung();
		$this->load->view('administrator/index',$data);
	}
	
	  public function getRak(){ 
		// POST data 
		$postData = $this->input->post();
		
		// load model 
		$this->load->model('m_document');
		
		// get data 
		$data = $this->m_document->getRak($postData);
		echo json_encode($data); 
	  }
	
	  public function getPosisi(){ 
		// POST data 
		$postData = $this->input->post();
	
		// load model 
		$this->load->model('m_document');
		
		// get data 
		$data = $this->m_document->getPosisi($postData);
		echo json_encode($data); 
	  }

	// function get_sub_category(){
	// 	$category_id = $this->input->post('id',TRUE);
	// 	$data = $this->m_document->get_sub_category($category_id)->result();
	// 	echo json_encode($data);
	// }

	// function get_sub_sub_category(){
	// 	$category_id = $this->input->post('id',TRUE);
	// 	$data = $this->m_document->get_sub_sub_category($category_id)->result();
	// 	echo json_encode($data);
	// }

	private function getKategori($id){
		$this->db->select('nama_kategori');
		$this->db->from('kategori');
		$this->db->where('id_kategori',$id);
		$query = $this->db->get();
		$result = $query->row();
		return $result->nama_kategori;
	}

	private function getIDposisi($baris,$kolom){
		$this->db->select('id_posisi');
		$this->db->from('posisi');
		$this->db->where('baris',$baris)->where('kolom',$kolom);
		$query = $this->db->get();
		$result = $query->row();
		return $result->id_posisi;
	}

	// public function test(){
	// 	echo $this->getIDposisi('2','b');
	// }

	public function do_upload(){	
		$pemilik = $this->input->post('pemilik');	
		$alamat = $this->input->post('alamat');
		$tahun = $this->input->post('tahun');	
		$kategori = $this->input->post('kategori');	
		$jeniskdr = $this->input->post('jeniskdr');	
		$keterangan = $this->input->post('keterangan');	
		$nopolisi = $this->input->post('nopolisi');	
		$nopolisi_name = str_replace(" ","",$nopolisi);

		$norangka = $this->input->post('norangka');	
		$nomesin = $this->input->post('nomesin');
		$creater = $this->input->post('creater');
		// echo $nomesin;
		$baris = $this->input->post('baris');
		$kolom = $this->input->post('kolom');
		$lokasi = $this->getIDposisi($baris,$kolom);

		$nama_kategori = $this->getKategori($kategori);	
		// echo $nama_kategori;
		$config['upload_path']          = './uploads/'.$nama_kategori."/";
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']  			= $nama_kategori."_".$nopolisi_name.date("Y-m-d_h_i_sa");;
		$config['max_size']             = 8192;
		$config['max_width']            = 6000;
		$config['max_height']           = 6000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload('berkas')){
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
		}else{
			$data = array('upload_data' => $this->upload->data("file_name"));
			
		}
		$name_doc =  $data["upload_data"];

		$data_doc = array(
			'nama_pemilik' => $pemilik,
			'alamat' => $alamat,
			'tahun' => $tahun,
			'id_kategori' => $kategori,
			'id_kendaraan' => $jeniskdr,
			'keterangan' => $keterangan,
			'nopolis' => $nopolisi,
			'norangka' => $norangka,
			'nomesin' => $nomesin,
			'name_doc' => $name_doc,
			'lokasi' => $lokasi,
			'creater' => $creater
			);
		$this->m_document->input_data($data_doc,'documents');
		redirect('documents/index');

		// var_dump($data);
		// $data['page'] = 'documents/add';	
		// $this->load->view('administrator/index',$data);
	}
	public function detail(){
		$id = $this->uri->segment('3');
		$data['tb_doc'] = $this->db->query("SELECT * FROM documents AS D, kategori AS Ka, kendaraan AS Ke, posisi AS Po WHERE D.id_kategori = Ka.id_kategori AND D.id_kendaraan = Ke.id_kendaraan AND D.lokasi = Po.id_posisi AND D.id_doc = '$id'")->result();	
		$data['tb_loc'] = $this->db->query("SELECT * FROM documents AS D, posisi AS P, Rak AS R, Gedung AS G WHERE D.lokasi = P.id_posisi AND P.id_rak = R.id_rak AND R.id_gedung = G.id_gedung AND D.id_doc = '$id'")->result();
		// var_dump($data['tb_doc']);	
		$data['page'] = 'documents/detail';	
		$this->load->view('administrator/index',$data);
	}
	public function search(){
		$search = $this->input->post('search');
		//my sign
		// $data['dok'] = $this->db->query("SELECT * FROM documents AS D, kategori AS Ka, kendaraan AS Ke WHERE D.id_doc LIKE  '%$search%' or D.tahun LIKE  '%$search%' or 
		// D.nopolis LIKE  '%$search%' or D.nama_pemilik LIKE  '%$search%' or D.tahun LIKE  '%$search%' or D.keterangan LIKE  '%$search%' or
		// D.norangka LIKE  '%$search%' or D.nomesin LIKE  '%$search%' or D.name_doc LIKE  '%$search%'")->result();
		$data['dok'] = $this->db->query("SELECT * FROM documents WHERE id_doc LIKE  '%$search%' or tahun LIKE  '%$search%' or 
		nopolis LIKE  '%$search%' or nama_pemilik LIKE  '%$search%' or tahun LIKE  '%$search%' or keterangan LIKE  '%$search%' or
		norangka LIKE  '%$search%' or nomesin LIKE  '%$search%' or name_doc LIKE  '%$search%'")->result();
		
		$data['search'] = $search;	
		$data['page'] = 'documents/search';	
		$this->load->view('administrator/index',$data);
	}
	
	public function edit(){	
		$id	= $this->uri->segment('3');
		$data['page'] = 'documents/edit';
		// $data['tb_doc'] = $this->db->query("SELECT * FROM documents NATURAL JOIN kategori NATURAL JOIN kendaraan WHERE id_doc='$id'")->result();	
		$data['tb_doc'] = $this->db->query("SELECT * FROM documents AS D, kategori AS Ka, kendaraan AS Ke WHERE D.id_kategori = Ka.id_kategori AND D.id_kendaraan = Ke.id_kendaraan AND D.id_doc = '$id'")->result();	
		$data['tb_loc'] = $this->db->query("SELECT * FROM documents AS D, posisi AS P, Rak AS R, Gedung AS G WHERE D.lokasi = P.id_posisi AND P.id_rak = R.id_rak AND R.id_gedung = G.id_gedung AND D.id_doc = '$id'")->result();
		$data['tb_kategori'] = $this->db->query("SELECT * FROM kategori")->result();
		$data['tb_kendaraan'] = $this->db->query("SELECT * FROM kendaraan")->result();
		$data['cities'] = $this->m_document->getGedung();
		$this->load->view('administrator/index',$data);
	}
 
	public function do_edit(){	
		$id = $this->uri->segment('3');

		$pemilik = $this->input->post('pemilik');	
		$alamat = $this->input->post('alamat');
		$tahun = $this->input->post('tahun');	
		$kategori = $this->input->post('kategori');	
		$jeniskdr = $this->input->post('jeniskdr');	
		$keterangan = $this->input->post('keterangan');	
		$nopolisi = $this->input->post('nopolisi');	
		$nopolisi_name = str_replace(" ","",$nopolisi);

		$norangka = $this->input->post('norangka');	
		$nomesin = $this->input->post('nomesin');
		// echo $nomesin;
		$baris = $this->input->post('baris');
		$kolom = $this->input->post('kolom');
		$lokasi = $this->getIDposisi($baris,$kolom);

		$nama_kategori = $this->getKategori($kategori);	
		// echo $nama_kategori;
		$config['upload_path']          = './uploads/'.$nama_kategori."/";
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']  			= $nama_kategori."_".$nopolisi_name.date("Y-m-d_h_i_sa");;
		$config['max_size']             = 8192;
		$config['max_width']            = 6000;
		$config['max_height']           = 6000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload('berkas')){
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
		}else{
			$data = array('upload_data' => $this->upload->data("file_name"));
			
		}
		$name_doc =  $data["upload_data"];

		$data_doc = array(
			'nama_pemilik' => $pemilik,
			'alamat' => $alamat,
			'tahun' => $tahun,
			'id_kategori' => $kategori,
			'id_kendaraan' => $jeniskdr,
			'keterangan' => $keterangan,
			'nopolis' => $nopolisi,
			'norangka' => $norangka,
			'nomesin' => $nomesin,
			'lokasi' => $lokasi,
			'name_doc' => $name_doc
			);
		$this->m_document->update_data($data_doc,$id);
		redirect('documents/index');

		// var_dump($data);
		// $data['page'] = 'documents/add';	
		// $this->load->view('administrator/index',$data);
	}
	
	public function delete(){
		$id = $this->uri->segment('3');
		$this->m_document->insertToRestore($id);
		redirect('documents/index');
	}
}
