<?php 

class M_document extends CI_Model{
	function tampil_data(){
		return $this->db->get('documents');
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function update_data($data, $id){
        $this->db->where('id_doc', $id);
        $this->db->limit(1);
        $this->db->update('documents', $data);
	}
	
	function insertToRestore($id){
		$this->db->where('id_doc',$id);
		$q = $this->db->get('documents')->result();
		foreach ($q as $r){
			$this->db->insert('restore_documents',$r);
		}
		$this->delete_data($id);
	}

	function delete_data($id){
        $this->db->where('id_doc', $id);
        $this->db->delete('documents');
	}
	
	function getGedung(){

		$response = array();
	 
		// Select record
		$this->db->select('*');
		$q = $this->db->get('gedung');
		$response = $q->result_array();
	
		return $response;
	  }
	
	  // Get City departments
	  function getRak($postData){
		$response = array();
	 
		// Select record
		$this->db->select('id_rak,nama_rak');
		$this->db->where('id_gedung', $postData['gedung']);
		$q = $this->db->get('rak');
		$response = $q->result_array();
	
		return $response;
	  }
	
	  // Get Department user
	  function getPosisi($postData){
		$response = array();
	 
		// Select record
		$this->db->select('id_posisi,baris,kolom');
		$this->db->where('id_rak', $postData['rak']);
		$q = $this->db->get('posisi');
		$response = $q->result_array();
	
		return $response;
	  }
}