<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <div class="d-inline">
                            <h4>Documents</h4>
                            <span>penulisan data dokumen</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Documents</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Add Document</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5> Form Inputs</h5>
                <span>Harap <code>teliti</code> dalam <code>input</code> data</span>


                <div class="card-header-right">
                    <i class="icofont icofont-spinner-alt-5"></i>
                </div>

            </div>
            <div class="card-block">
                <h4 class="sub-title">Document Information</h4>
                <form action = "<?php echo base_url() ?>index.php/documents/do_upload" method="POST" enctype="multipart/form-data">
                    
                    <input type="hidden" name="creater" value="<?php echo $this->session->userdata('id_user'); ?>">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Pemilik</label>
                        <div class="col-sm-10">
                            <input type="text" name ="pemilik" class="form-control" placeholder="Nama Pemilik" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name ="alamat" class="form-control" placeholder="Alamat" required></textarea>
                        </div>
                    </div>
 
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tahun Pembuatan</label>
                        <div class="col-sm-10">
                            <input type="text" name ="tahun" class="form-control" placeholder="Tahun Pembuatan" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kategori</label>
                        <div class="col-sm-10">
                            <select name="kategori" class="form-control">
                                <option value="">Pilih salah satu</option>
                                <?php foreach ($tb_kategori as $value) :?>
                                    <option value="<?php echo $value->id_kategori ?>"><?php echo $value->nama_kategori ?></option>
                                <?php endforeach ?>
                                <!-- <option value="BPKB">BPKB</option>
                                <option value="STNK">STNK</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Kendaraan</label>
                        <div class="col-sm-10">
                            <select name="jeniskdr" class="form-control">
                                <option value="">Pilih salah satu</option>
                                <?php foreach ($tb_kendaraan as $value) :?>
                                    <option value="<?php echo $value->id_kendaraan ?>"><?php echo $value->jenis_kdr ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>   
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name ="keterangan" class="form-control" placeholder="keterangan" required></textarea>
                        </div>
                    </div>
                <!-- </form> -->
                <!-- <br>
                <h4 class="sub-title">Assigment Notification</h4> -->
                        <!-- <form> -->
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Individuals</label>
                            <div class="col-sm-10">
                                <select name="cat_individual" class="form-control">
                                    <option value="opt1">Select One Value Only</option>
                                    <option value="opt2">Type 2</option>
                                    <option value="opt3">Type 3</option>
                                    <option value="opt4">Type 4</option>
                                    <option value="opt5">Type 5</option>
                                    <option value="opt6">Type 6</option>
                                    <option value="opt7">Type 7</option>
                                    <option value="opt8">Type 8</option>
                                </select>
                            </div>
                        </div>  

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Groups</label>
                            <div class="col-sm-10">
                                <select name="cat_group" class="form-control">
                                    <option value="opt1">Select One Value Only</option>
                                    <option value="opt2">Type 2</option>
                                    <option value="opt3">Type 3</option>
                                    <option value="opt4">Type 4</option>
                                    <option value="opt5">Type 5</option>
                                    <option value="opt6">Type 6</option>
                                    <option value="opt7">Type 7</option>
                                    <option value="opt8">Type 8</option>
                                </select>
                            </div>
                        </div> -->
                        <h4 class="sub-title">Documents Properties</h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Polisi</label>
                            <div class="col-sm-10">
                                <input type="text" name ="nopolisi" class="form-control" placeholder="Nomor Polisi" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Rangka</label>
                            <div class="col-sm-10">
                                <input type="text" name ="norangka" class="form-control" placeholder="Nomor Rangka" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Mesin</label>
                            <div class="col-sm-10">
                                <input type="text" name ="nomesin" class="form-control" placeholder="Nomor Mesin" required>
                            </div>
                        </div>
                        <h4 class="sub-title">Documents Location</h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Gedung</label>
                            <div class="col-sm-10">
                                <select name="jeniskdr" id="sel_gedung" class="form-control">
                                    <option value="">Pilih salah satu</option>
                                    <?php
                                        foreach($cities as $city){
                                            echo "<option value='".$city['id_gedung']."'>".$city['nama_gedung']."</option>";
                                    }
                                    ?>
                                    <!-- PR: picker lokasi sesuai option sebelumnya -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Rak</label>
                            <div class="col-sm-10">
                                <select name="jeniskdr" id="sel_rak" class="form-control">
                                    <option value="">Pilih salah satu</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kolom</label>
                            <div class="col-sm-10">
                                <select name="kolom" id="sel_kolom" class="form-control">
                                    <option value="">Pilih salah satu</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Baris</label>
                            <div class="col-sm-10">
                                <select name="baris" id="sel_baris" class="form-control">
                                    <option value="">Pilih salah satu</option>
                                </select>
                            </div>
                        </div>
                        <h4 class="sub-title">Documents Uploads</h4>
                            <!-- <div class="sub-title">Example 2</div> -->
                             <input type="file" name="berkas">
                             <button type="submit" class="btn btn-success m-b-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <select id="type">
    <option value="item0">--Select an Item--</option>
    <option value="item1">item1</option>
    <option value="item2">item2</option>
    <option value="item3">item3</option>
</select>

<select id="size">
    <option value="">-- select one -- </option>
</select>

<script>
    $(document).ready(function () {
    $("#type").change(function () {
        var val = $(this).val();
        if (val == "item1") {
            $("#size").html("<option value='test'>item1: test 1</option><option value='test2'>item1: test 2</option>");
        } else if (val == "item2") {
            $("#size").html("<option value='test'>item2: test 1</option><option value='test2'>item2: test 2</option>");
        } else if (val == "item3") {
            $("#size").html("<option value='test'>item3: test 1</option><option value='test2'>item3: test 2</option>");
        } else if (val == "item0") {
            $("#size").html("<option value=''>--select one--</option>");
        }
    });
});
</script> -->

  <!-- Script -->   
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-1.10.2.min.js'?>"></script>
  <script type='text/javascript'>
  // baseURL variable
  var baseURL= "<?php echo base_url();?>";
 
  $(document).ready(function(){
 
    // City change
    $('#sel_gedung').change(function(){
      var gedung = $(this).val();

      // AJAX request
      $.ajax({
        url:'<?=base_url()?>index.php/documents/getRak',
        method: 'post',
        data: {gedung: gedung},
        dataType: 'json',
        success: function(response){

          // Remove options 
          $('#sel_kolom').find('option').not(':first').remove();
          $('#sel_rak').find('option').not(':first').remove();

          // Add options
          $.each(response,function(index,data){
             $('#sel_rak').append('<option value="'+data['id_rak']+'">'+data['nama_rak']+'</option>');
          });
        }
     });
   });
 
   // Department change
   $('#sel_rak').change(function(){
     var rak = $(this).val();

     // AJAX request
     $.ajax({
       url:'<?=base_url()?>index.php/documents/getPosisi',
       method: 'post',
       data: {rak: rak},
       dataType: 'json',
       success: function(response){
 
         // Remove options
         $('#sel_kolom').find('option').not(':first').remove();
         $('#sel_baris').find('option').not(':first').remove();

         // Add options
         $.each(response,function(index,data){
           $('#sel_kolom').append('<option value="'+data['kolom']+'">'+data['kolom']+'</option>');
           $('#sel_baris').append('<option value="'+data['baris']+'">'+data['baris']+'</option>');
         });
       }
    });
  });
 
 });
 </script>
