<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Search Result :</h4>
                    <span>Beberpa atribut dokument digunakan untuk pencarian</span>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Documents</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">List Documents</a>
                    </li>
                </ul>
            </div>
        </div> -->
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-md-12">
        <div class="card">
            <div class="card-block">
                <!-- Row start -->
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <!-- <div class="sub-title">Default</div> -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab" aria-expanded="true">Database Search</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab" aria-expanded="false">Full text search</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabs card-block">
                            <div class="tab-pane active" id="home1" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">search for : </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="search" value = "<?php echo $search; ?>" id="integer" placeholder="">
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            <select name="category" class="form-control">
                                                <option value="">All words</option>
                                                <option value="depan">depan</option>
                                                <option value="belakang">belakang</option>
                                                <option value="tangah">tengah</option>
                                            </select>
                                        </div>
                                    </div>   
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">search in : </label>
                                        <div class="col-sm-8">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Pemilik</span>
                                                </label>
                                            </div>
                                            <br>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Keterangan</span>
                                                </label>
                                            </div>
                                            <br>

                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Atribut</span>
                                                </label>
                                            </div>
                                            <br>

                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>ID</span>
                                                </label>
                                            </div>
                                            <br>

                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Nomor Polisi</span>
                                                </label>
                                            </div>
                                            <br>

                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Nomor Rangka</span>
                                                </label>
                                            </div>
                                            <br>

                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="" checked>
                                                    <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                    </span>
                                                    <span>Nomor Mesin</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Pemilik : </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="nama_pemeilik" value="" placeholder="Nama Pemilik">
                                            <span class="messages"></span>
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Uplouder : </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="uplouder" value="administrator" placeholder="Uplouder">
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Search Result</label>
                                        <div class="col-sm-8">
                                            <select name="category" class="form-control">
                                                <option value="DNF">Document and Folder</option>
                                                <option value="History">History</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">In Folder </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="integer" id="integer" value="STNK/ and BPKB/" disabled>
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                
                            </div>
                            </div>
                            <div class="tab-pane" id="profile1" role="tabpanel" aria-expanded="false">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">search for : </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="search" value = "<?php echo $search; ?>" placeholder="">
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
        </div>
       
    </div>
    <div class="col-xl-8 col-md-12">
        <!-- Bootstrap tab card start -->
         <div class="card user-card-full">
            <div class="row m-l-0 m-r-0">
                <div class="col-sm-12">
                    
                    <div class="card-block">
                        <div class="alert alert-success border-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled"></i>
                            </button>
                            <strong>Success!</strong> Found <code><?php echo count($dok);?> Document</code>
                        </div>
                        <!-- modals -->
                        <!-- <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#large-Modal">Large</button>
                        <div class="modal fade" id="large-Modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Modal title</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h5>Default Modal</h5>
                                        <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Document Information</h6> -->
                        <div class="row">
                        <div class="table-responsive">
                            <table class="table table-hover table-borderless">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Attributes</th>
                                        <th>Status</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach ($dok as $value) {
                                    # code...
                                    if ($value->id_kategori==1) {
                                        $path = "uploads/STNK/";
                                    }
                                    else if ($value->id_kategori==2) {
                                        $path = "uploads/BPKB/";                                        
                                    }
                                    
                                ?>
                                    <tr>
                                        <td>
                                        <div class="row m-b-25">
                                                    <div class="col-auto p-r-0">
                                                        <a href="" data-toggle="modal" data-target="#<?php echo $value->id_doc;?>">

                                                        <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="" class="img-fluid img-100">
                                                        </a>
                                                    </div>
                                                    <div class="col">
                                                        <h6 class="m-b-5"><?php echo $value->name_doc;?></h6>
                                                        <p class="text-c-blue m-b-0">Upload By administrator</p>
                                                    </div>
                                                </div>
                                        <!-- <img src="<?php echo base_url() ?>asset/files/assets/images/widget/GERMANY.jpg" alt="" class="img-fluid img-30"></td> -->
                                        <td>-</td>
                                        <td>Release</td>
                                        <td class="text-right">
                                        <a href="#!" class="b-b-danger text-danger">Hapus </a> &nbsp;
                                        <a href="#!" class="b-b-primary text-primary">Edit</a><br>
                                        
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="<?php echo $value->id_doc;?>" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Detail Document</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- <h5>Default Modal</h5> -->
                                                    <!-- <h6 class="m-b-5"><?php echo $value->name_doc;?></h6> -->
                                                    <div class="row">                                                    
                                                        <div class="col-lg-6 col-sm-6">
                                                            <div class="thumbnail">
                                                                <div class="thumb">
                                                                    <a href="<?php echo base_url().$path.$value->name_doc; ?>" target="_blank" data-lightbox="1" data-title="My caption 1">
                                                                        <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="" class="img-fluid img-thumbnail">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6">
                                                        <h6 class="sub-title">Document Information</h6>
                                                            <ul class="basic-list list-icons">
                                                                <li>
                                                                    <i class="icofont icofont-user-alt-3 text-primary p-absolute text-center d-block f-30"></i>
                                                                    <h6><b>Nama Pemilik</b></h6>
                                                                    <p><?php echo $value->nama_pemilik;?></p>
                                                                </li>
                                                                <li>
                                                                    <h6><b>Alamat</b></h6>
                                                                    <i class="icofont icofont-map-pins text-primary p-absolute text-center d-block f-30"></i>
                                                                    <p><?php echo $value->alamat;?></p>
                                                                </li>
                                                                <!-- <li>
                                                                    <h6><b>Kategori</b></h6>
                                                                    <i class="icofont icofont-bell-alt text-primary p-absolute text-center d-block f-30"></i>
                                                                    <p><?php echo $value->kategori;?></p>
                                                                </li> -->
                                                                <li>
                                                                    <h6><b>Nomor Polisi</b></h6>
                                                                    <i class="icofont icofont-ui-v-card text-primary p-absolute text-center d-block f-30"></i>
                                                                    <p><?php echo $value->nopolis;?></p>
                                                                </li>
                                                                <li>
                                                                <a href="<?php echo base_url() ?>index.php/documents/detail/<?php echo $value->id_doc; ?>" class="b-b-primary text-primary">Selengkapnya </a>
                                                                    
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p> -->
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                           
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap tab card end -->
    </div>
</div>
<div class="col-md-12 col-lg-4">
    
</div>