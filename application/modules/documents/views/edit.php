<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <div class="d-inline">
                            <h4>Documents</h4>
                            <span>penulisan data dokumen</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Documents</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Add Document</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5> Form Inputs</h5>
                <span>Harap <code>teliti</code> dalam <code>input</code> data</span>

                <?php foreach ($tb_doc as $doc){ ?>

                <div class="card-header-right">
                    <i class="icofont icofont-spinner-alt-5"></i>
                </div>

            </div>
            <div class="card-block">
                <h4 class="sub-title">Document Information</h4>
                <form action = "<?php echo base_url() ?>index.php/documents/do_edit/<?php echo $doc->id_doc; ?>" method="POST" enctype="multipart/form-data">
                    
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Pemilik</label>
                        <div class="col-sm-10">
                            <input type="text" name ="pemilik" class="form-control" placeholder="Nama Pemilik" value="<?php echo $doc->nama_pemilik ?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name ="alamat" class="form-control" placeholder="Alamat" value="<?php echo $doc->alamat ?>" required><?php echo $doc->alamat ?></textarea>
                        </div>
                    </div>
 
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tahun Pembuatan</label>
                        <div class="col-sm-10">
                            <input type="text" name ="tahun" class="form-control" placeholder="Tahun Pembuatan" value="<?php echo $doc->tahun ?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kategori</label>
                        <div class="col-sm-10">
                            <select name="kategori" class="form-control">
                                <option value="<?php echo $doc->id_kategori ?>"><?php echo $doc->nama_kategori ?></option>
                                <?php foreach ($tb_kategori as $value) :?>
                                    <?php if ($value->id_kategori == $doc->id_kategori) continue ?>
                                    <option value="<?php echo $value->id_kategori ?>"><?php echo $value->nama_kategori ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Kendaraan</label>
                        <div class="col-sm-10">
                            <select name="jeniskdr" class="form-control">
                                <option value="<?php echo $doc->id_kendaraan ?>"><?php echo $doc->jenis_kdr ?></option>
                                <?php foreach ($tb_kendaraan as $value) :?>
                                    <?php if ($value->id_kendaraan == $doc->id_kendaraan) continue ?>
                                    <option value="<?php echo $value->id_kendaraan ?>"><?php echo $value->jenis_kdr ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>   
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name ="keterangan" class="form-control" placeholder="keterangan" value="<?php echo $doc->keterangan ?>" required><?php echo $doc->keterangan ?></textarea>
                        </div>
                    </div>
                <!-- </form> -->
                <!-- <br>
                <h4 class="sub-title">Assigment Notification</h4> -->
                        <!-- <form> -->
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Individuals</label>
                            <div class="col-sm-10">
                                <select name="cat_individual" class="form-control">
                                    <option value="opt1">Select One Value Only</option>
                                    <option value="opt2">Type 2</option>
                                    <option value="opt3">Type 3</option>
                                    <option value="opt4">Type 4</option>
                                    <option value="opt5">Type 5</option>
                                    <option value="opt6">Type 6</option>
                                    <option value="opt7">Type 7</option>
                                    <option value="opt8">Type 8</option>
                                </select>
                            </div>
                        </div>  

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Groups</label>
                            <div class="col-sm-10">
                                <select name="cat_group" class="form-control">
                                    <option value="opt1">Select One Value Only</option>
                                    <option value="opt2">Type 2</option>
                                    <option value="opt3">Type 3</option>
                                    <option value="opt4">Type 4</option>
                                    <option value="opt5">Type 5</option>
                                    <option value="opt6">Type 6</option>
                                    <option value="opt7">Type 7</option>
                                    <option value="opt8">Type 8</option>
                                </select>
                            </div>
                        </div> -->
                        <h4 class="sub-title">Documents Properties</h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Polisi</label>
                            <div class="col-sm-10">
                                <input type="text" name ="nopolisi" class="form-control" placeholder="Nomor Polisi" value="<?php echo $doc->nopolis ?>" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Rangka</label>
                            <div class="col-sm-10">
                                <input type="text" name ="norangka" class="form-control" placeholder="Nomor Rangka" value="<?php echo $doc->norangka ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Mesin</label>
                            <div class="col-sm-10">
                                <input type="text" name ="nomesin" class="form-control" placeholder="Nomor Mesin" value="<?php echo $doc->nomesin ?>" required>
                            </div>
                        </div>
                        <h4 class="sub-title">Documents Location</h4>
                        <?php foreach($tb_loc as $loc) {?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Gedung</label>
                            <div class="col-sm-10">
                                <select name="jeniskdr" id="sel_gedung" class="form-control">
                                    <option value=<?php echo $loc->id_gedung;?>><?php echo $loc->nama_gedung?></option>
                                    <?php
                                        foreach($cities as $city){
                                            echo "<option value='".$city['id_gedung']."'>".$city['nama_gedung']."</option>";
                                    }
                                    ?>
                                    <!-- PR: picker lokasi sesuai option sebelumnya -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Rak</label>
                            <div class="col-sm-10">
                                <select name="jeniskdr" id="sel_rak" class="form-control">
                                    <option value=<?php echo $loc->id_rak; ?>><?php echo $loc->nama_rak;?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kolom</label>
                            <div class="col-sm-10">
                                <select name="kolom" id="sel_kolom" class="form-control">
                                    <option value="<?php echo $loc->kolom;?>"><?php echo $loc->kolom;?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Baris</label>
                            <div class="col-sm-10">
                                <select name="baris" id="sel_baris" class="form-control">
                                    <option value="<?php echo $loc->baris;?>"><?php echo $loc->baris;?></option>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                        <h4 class="sub-title">Documents Uploads</h4>
                            <!-- <div class="sub-title">Example 2</div> -->
                             <!-- the problem -->
                             <input type="file" value="<?php echo $doc->name_doc ?>" name="berkas" >
                             <label><?php echo $doc->name_doc ?></label>
                             <button type="submit" class="btn btn-success m-b-0">Submit</button>
                        </form>
                <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-1.10.2.min.js'?>"></script>
  <script type='text/javascript'>
  // baseURL variable
  var baseURL= "<?php echo base_url();?>";
 
  $(document).ready(function(){
 
    // City change
    $('#sel_gedung').change(function(){
      var gedung = $(this).val();

      // AJAX request
      $.ajax({
        url:'<?=base_url()?>index.php/documents/getRak',
        method: 'post',
        data: {gedung: gedung},
        dataType: 'json',
        success: function(response){

          // Remove options 
          $('#sel_kolom').find('option').not(':first').remove();
          $('#sel_rak').find('option').not(':first').remove();

          // Add options
          $.each(response,function(index,data){
             $('#sel_rak').append('<option value="'+data['id_rak']+'">'+data['nama_rak']+'</option>');
          });
        }
     });
   });
 
   // Department change
   $('#sel_rak').change(function(){
     var rak = $(this).val();

     // AJAX request
     $.ajax({
       url:'<?=base_url()?>index.php/documents/getPosisi',
       method: 'post',
       data: {rak: rak},
       dataType: 'json',
       success: function(response){
 
         // Remove options
         $('#sel_kolom').find('option').not(':first').remove();
         $('#sel_baris').find('option').not(':first').remove();

         // Add options
         $.each(response,function(index,data){
           $('#sel_kolom').append('<option value="'+data['kolom']+'">'+data['kolom']+'</option>');
           $('#sel_baris').append('<option value="'+data['baris']+'">'+data['baris']+'</option>');
         });
       }
    });
  });
 
 });
 </script>