<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Documents</h4>
                    <span>Detail Document</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index-1.htm"> <i class="feather icon-box"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Documents</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-md-12">
        <div class="card user-card-full">
            <div class="row m-l-0 m-r-0">
                <div class="col-sm-12">
                    <div class="card-block">
                    <?php 
                        foreach ($tb_doc as $value) {
                            # code...
                            if ($value->nama_kategori=='STNK') {
                                $path = "uploads/STNK/";
                            }
                            else if ($value->nama_kategori=='BPKB') {
                                $path = "uploads/BPKB/";                                        
                            }
                            // echo base_url().$path.$value->name_doc;
                        ?>
                        <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Document Information</h6>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Nama Pemilik</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->nama_pemilik;
                                ?>
                                </h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Tahun</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->tahun;
                                ?></h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Kategori</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->nama_kategori;
                                ?>
                                </h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Jenis Kendaraan</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->jenis_kdr;
                                ?>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-b-10 f-w-600">Alamat</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->alamat;
                                ?>
                                </h6>
                                </h6>
                            </div>
                            <!-- <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Alamat</p>
                                <h6 class="text-muted f-w-400">0023-333-526136</h6>
                            </div> -->
                        </div>
                        <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Document Properties</h6>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Nomor Polisi</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->nopolis;
                                ?>
                                </h6>
                                </h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Nomor Mesin </p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->nomesin;
                                ?>
                                </h6>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Nomor Rangka</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $value->norangka;
                                ?>
                                </h6>
                                </h6>
                            </div>
                        </div>
                        <?php foreach ($tb_loc as $loc) {?>
                        <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Document location</h6>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Gedung</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $loc->nama_gedung;
                                ?>
                                </h6>
                                </h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Rak </p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $loc->nama_rak;
                                ?>
                                </h6>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Kolom</p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $loc->kolom;
                                ?>
                                </h6>
                                </h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600">Baris </p>
                                <h6 class="text-muted f-w-400">
                                <?php 
                                echo $loc->baris;
                                ?>
                                </h6>
                                </h6>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php 
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8 col-md-12">
        <!-- Bootstrap tab card start -->
        <div class="card">
            <div class="card-block">
                <!-- Row start -->
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="sub-title">Detail</div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab" aria-expanded="true">Curent Version</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab" aria-expanded="false">Previous Version</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messages1" role="tab" aria-expanded="false">Attachment</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#settings1" role="tab" aria-expanded="false">Related Document</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabs card-block">
                            <div class="tab-pane active" id="home1" role="tabpanel" aria-expanded="true">
                            <div class="card-block">
                                <div class="table-responsive">
                                    <table class="table table-hover table-borderless">
                                        <thead>
                                            <tr>
                                                <th>File</th>
                                                <th>Keterangan</th>
                                                <th>Status</th>
                                                <th class="text-right"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        foreach ($tb_doc as $value) {
                                            # code...
                                            if ($value->nama_kategori=='STNK') {
                                                $path = "uploads/STNK/";
                                            }
                                            else if ($value->nama_kategori=='BPKB') {
                                                $path = "uploads/BPKB/";                                        
                                            }
                                            // echo base_url().$path.$value->name_doc;
                                        ?>
                                            <tr>
                                                <td>
                                                <div class="row m-b-25">
                                                    <div class="col-auto p-r-0">
                                                        <a href="" data-toggle="modal" data-target="#<?php echo $value->id_doc;?>">

                                                        <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="" class="img-fluid img-100">
                                                        </a>
                                                    </div>
                                                            <!-- <div class="col-auto p-r-0">
                                                                <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="" class="img-fluid img-100">
                                                            </div> -->
                                                            <div class="col">
                                                                <p class="m-b-5"><?php echo $value->name_doc; ?></p>
                                                                <p class="text-c-blue m-b-0">Upload By administrator</p>
                                                            </div>
                                                        </div>
                                                <!-- <img src="<?php echo base_url() ?>asset/files/assets/images/widget/GERMANY.jpg" alt="" class="img-fluid img-30"></td> -->
                                                <td><?php echo $value->keterangan; ?></td>
                                                <td>Release</td>
                                                <td class="text-right">
                                                <a href="#!" class="b-b-primary text-primary">Download </a><br>
                                                <a href="#!" class="b-b-primary text-primary">View Online </a><br>
                                                <a href="#!" class="b-b-primary text-primary">Change Status </a><br>
                                                <a href="#!" class="b-b-primary text-primary">Set Workflow </a><br>
                                                <a href="#!" class="b-b-primary text-primary">Edit</a><br>
                                                
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="<?php echo $value->id_doc;?>" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Detail Document</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- <h5>Default Modal</h5> -->
                                                            <!-- <h6 class="m-b-5"><?php echo $value->name_doc;?></h6> -->
                                                            <div class="row">                                                    
                                                                <div class="col-lg-12 col-sm-12">
                                                                    <div class="thumbnail">
                                                                        <div class="thumb">
                                                                            <a href="<?php echo base_url().$path.$value->name_doc; ?>" target="_blank" data-lightbox="1" data-title="My caption 1">
                                                                                <img src="<?php echo base_url().$path.$value->name_doc; ?>" alt="" class="img-fluid img-thumbnail">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               
                                                            </div>
                                                            <!-- <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p> -->
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                        }
                                        ?>
                                        </tbody> 
                                    </table>
                                </div>
                                <!-- <div class="text-right  m-r-20">
                                    <a href="#!" class="b-b-primary text-primary">View all Sales Locations </a>
                                </div> -->
                            </div>
                            </div>
                            <div class="tab-pane" id="profile1" role="tabpanel" aria-expanded="false">
                                <p class="m-0">2.Cras consequat in enim ut efficitur. Nulla posuere elit quis auctor interdum praesent sit amet nulla vel enim amet. Donec convallis tellus neque, et imperdiet felis amet.</p>
                            </div>
                            <div class="tab-pane" id="messages1" role="tabpanel" aria-expanded="false">
                                <p class="m-0">3. This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean mas Cum sociis natoque penatibus et magnis dis.....</p>
                            </div>
                            <div class="tab-pane" id="settings1" role="tabpanel" aria-expanded="false">
                                <p class="m-0">4.Cras consequat in enim ut efficitur. Nulla posuere elit quis auctor interdum praesent sit amet nulla vel enim amet. Donec convallis tellus neque, et imperdiet felis amet.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
        </div>
        <!-- Bootstrap tab card end -->
    </div>
</div>